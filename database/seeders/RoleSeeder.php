<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol1=Role::create(['name' => 'Admin']);
        $rol2=Role::create(['name' => 'SuperU']);
        $rol3=Role::create(['name' => 'SuperV']);

        $permission = Permission::create(['name' => 'Admin.home'])->syncRoles([$rol1,$rol2,$rol3]);

        $permission = Permission::create(['name' => 'login.index.index'])->syncRoles([$rol1,$rol2,$rol3]);
        $permission = Permission::create(['name' => 'login.index.create'])->syncRoles([$rol1,$rol2]);
        $permission = Permission::create(['name' => 'login.index.edit'])->assignRole($rol1);
        $permission = Permission::create(['name' => 'login.index.destroy'])->assignRole($rol1);



    }
}
