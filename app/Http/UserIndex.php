<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\User;
use Livewire\withPagination;

class UserIndex extends Component
{
    use withPagination;

    protected $paginationTheme="bootstrap";
    public function render()
    {
        $user=User::paginate();
        return view('resources.views.user-index',compact('user'));
    }
}
