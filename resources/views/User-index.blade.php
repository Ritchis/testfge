<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <table class="table table-striped border 1 center">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>    Name</th>
                                <th>    Correo</th>
                                <th>    Operaciones</th>
                            </tr>
                        </thead>
                        <tbody class="border 2">
                            @foreach($user as $user)
                            <tr>
                                <th>{{$user->id }}</th>
                                <th>{{$user->name }}</th>
                                <th>{{$user->email }}</th>
                                <th><a class="btn btn-primary" 
                                href="{{route('user.edit',$user)}}">Editar</th>
                                <th><a class="btn btn-primary" 
                                href="{{route('user.destroy',$user)}}">Eliminar</th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>