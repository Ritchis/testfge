<nav x-data="{ open: false }" class="bg-white border-b border-gray-100">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">
                <!-- Logo -->
                <div class="flex-shrink-0 flex items-center">
                    <a href="{{ route('dashboard') }}">
                        <x-jet-application-mark class="block h-9 w-auto" />
                    </a>
                </div>
            </div>
            <a href="{{ route('register') }}">Registrar</a>
        </div>
    </div>
</nav>

<div class="grid">
    <div class= "container-fluid">
        <div class="col-md-3 offset-md-3r">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif{{ __('') }}
                    <table class="table table-striped border 1 center">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>    Name</th>
                                <th>    Correo</th>
                                <th>    Operaciones</th>
                            </tr>
                        </thead>
                        <tbody class="border 2">
                            @foreach($user as $user)
                            <tr>
                                <th>{{$user->id }}</th>
                                <th>{{$user->name }}</th>
                                <th>{{$user->email }}</th>
                                <th><a class="btn btn-primary" 
                                href="{{route('user.edit',$user)}}">Editar</th>
                                <th><a class="btn btn-primary" 
                                href="{{route('user.destroy',$user)}}">Eliminar</th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
            