<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\SessionsController;

Route::get('/', function () {
    return view('Home');
});
Route::get('/register',[RegisterController::class, 'create'])->name('register.index');
Route::resource('user',UserController::class)->name('*','login.home');
Route::post('/register',[RegisterController::class, 'store'])->name('register.store');
Route::get('/login',[LoginController::class, 'create'])->name('login.index');
Route::post('user/{user}',[UserController::class, 'update'])->name('login.index');
Route::Delete('user/{user}',[UserController::class, 'destroy'])->name('user.destroy'); 
Route::put('user/{user}',[UserController::class,'update'])->name('user.edit');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
